# coding: utf-8
require 'json'
require 'selenium-webdriver'
require 'headless'
require 'pry'
require 'logger'
require 'dotenv'
require 'active_support/all'
require './api_service'
require './gmail_scraper'
require './cookies'
require 'yaml'

# CARET_CLASS="gwt-Anchor.G6M3D1D-m-q.G6M3D1D-m-E.G6M3D1D-m-v"

Dotenv.load

module Adomik
  module GoogleAdexchange
    class WebScraper
      
      attr_reader :logger, :conf
      
      def initialize(conf)
        @@screenshot = 0
        @wait        = Selenium::WebDriver::Wait.new(:timeout => 300)
        @logger      = ::Logger::new(STDOUT)
        @conf        = YAML.load_file(conf)
        @logger.info "Scraper creation"

        profile = Selenium::WebDriver::Firefox::Profile.new
        profile['browser.download.folderList'] = 2
        profile['browser.download.dir']        = @conf['destdir']
        profile.native_events                  = true
        profile['browser.helperApps.neverAsk.saveToDisk'] = 'text/csv'
        
        @driver  = Selenium::WebDriver.for :firefox, profile: profile
        # @driver.manage.window.full_screen
        @driver.manage.window.maximize
        @driver.manage.timeouts.implicit_wait = 30
        @logger.info "Scraper creation done!"
      end
      
      def scrape(&block)
        @logger.info "Start scraping"
        yield self, @driver, @logger 
        @logger.info "Stop scraping"    
      end

      def wait_for type, value
        # some exception : Net::ReadTimeout
        @logger.info "Wait for type <#{type}> value <#{value}>"
        elt = @driver.find_element(type, value)
        # @wait.until { elt = @driver.find_element(type, value) }
        !60.times{
          elt = @driver.find_element(type, value)
          # binding.pry
          @logger.debug "wait for elt <#{elt}>"
          break if (elt.displayed? and elt.enabled? rescue false)
          sleep 2
        }
        @driver.save_screenshot("/tmp/screen-#{@@screenshot += 1}.png")
        @logger.info "Found elt <#{elt}>"
        js_script = "window.scrollTo(#{elt.location.x}, #{elt.location.y})"
        @logger.info "js_script: <#{js_script}>"
        # @driver.execute_script(js_script)
        elt
      rescue Net::ReadTimeout => e
        @logger.info "Timeout, RETRY <#{e}>"
        binding.pry
        retry
      rescue Selenium::WebDriver::Error::TimeOutError => e
        @logger.info 'Elt Not found: open a pry console'
        binding.pry
      end
      
      def self.scrape(conf, &block)
        # headless = Headless.new
        # headless.start
        new(conf).scrape(&block)
        # headless.destroy    
      end
      
    end
  end
end

semillo = { mail: ENV['SEMILLO_EMAIL'], pass: ENV['SEMILLO_PASS'] }
ebay    = { mail: ENV['EBAY_EMAIL'],    pass: ENV['EBAY_PASS'] }

creds         = 'gmail-oauth2-semillo.json'
params        = {date: '2016-01-24', userId: 'semiloadxpilot@gmail.com'}
# gmail_scraper = Adomik::GoogleAdexchange::GmailScraper.new(creds)

report_urls   = {
  "Ad Exchange Scheduled Report - Adomik_Standard_M" => "https://www.goog
le.com/adx/querytool/runScheduledQuery?action=VIEW&qid=tj8OokV8wMUzt_EIIQx7NNFlp6ClgZ9f-7rUB8evc9BoaTQZtpd_btYfT9z3MaFm&runAsOfDate=2016-01-24",
  "Ad Exchange Scheduled Report - Adomik_Bid_M"=> "https://www.google.com/adx/querytool/runScheduledQuery?action=VIEW&qid=fRBO2LbcKW7NmxuFDwW_7H6xWMg1xqVwFIaHeK4YdC7qqhUmc0lBdjN_35OijxgS&runAsOfDate=2016-01-24"}
# report_urls   = gmail_scraper.report_urls(params)

# exit
ENV['PAGER'] = nil

Adomik::GoogleAdexchange::WebScraper.scrape('conf.yml') do |this, driver, logger |
  
  base_url = "https://accounts.google.com/"
  driver.get(base_url + "/ServiceLogin#identifier")

  ### LOGIN ###
  logger.info "Login"
  driver.find_element(:id, "Email").send_keys semillo[:mail]
  driver.find_element(:id, "next").click

  ### PASS ###
  logger.info "Pass"  
  driver.find_element(:id, "Passwd").send_keys semillo[:pass]
  driver.find_element(:id, "signIn").click

  driver.get "https://www.google.com/adx"
  sleep 10
  # binding.pry
  # exit

  dom_elts = [{type: :link, value: 'Query Tool'},
              {type: :link, value: 'RUN NOW'},
              {type: :css,  value: ENV['CARET_CLASS']},
              {type: :id,   value: ENV['REPORT_ID']}]


  this.conf['reports'].each do |report_name|
    # step 1: query tool
    ## TODO: check if we allready on the query tool page. Use driver.current_url
    if driver.current_url != 'https://www.google.com/adx/Main.html#QUERY_TOOL'
      logger.info "Navigate to Query Tool page. Current url <#{driver.current_url}>"
      driver.navigate.to 'https://www.google.com/adx/Main.html#QUERY_TOOL'
    end
    # elt = this.wait_for(:link, 'Query Tool')
    # elt.click
    
    sleep 10
    driver.save_screenshot('query_tool.png')  
    run_links    = driver.find_elements(:xpath,'//a[@class="G6M3D1D-d-f"]')
    reports_name = driver.find_elements(:xpath,'//span[@class="lnk"]').map(&:text)
    ##
    
    idx = reports_name.find_index(report_name)
    if idx.nil?    
      logger.info("<#{report_name}> not available")
      next
    end
    logger.info("Click 'RUN NOW' for <#{report_name}> report")
    run_links[idx].click()
    sleep 10
    # binding.pry
    driver.save_screenshot('run_now.png')
    while true
      begin
        driver.find_element(:xpath, '//*[text()="Loading..."]')
        sleep 5
        logger.info "Loading...!"
      rescue Selenium::WebDriver::Error::NoSuchElementError => e
        logger.info "Loading done!"
        break
      end
    end

    begin
      xpath = '//*[text()="This report has no data"]'
      elt   = driver.find_element(:xpath, xpath)
      msg = "Report <#{report_name}> contains no data. Skipping: "
      msg += "<#{elt}>"
      if elt.displayed? and elt.enabled?
        logger.info msg
        next
      end
      raise Selenium::WebDriver::Error::NoSuchElementError
    rescue Selenium::WebDriver::Error::NoSuchElementError => e
      logger.info "Report <#{report_name}> contains data. Exporting"
    end
    
    # elt = driver.find_element(:xpath, ) #
    elt = this.wait_for(:xpath, '//div[@class="G6M3D1D-m-y G6M3D1D-m-bb"]') # ENV['CARET_CLASS'])
    elt.click
    sleep 5
    driver.save_screenshot('carret.png')
    # elt = driver.find_element(:xpath, '//div[@id="AdxReportViewImpl-TSV"]')
    elt = this.wait_for(:xpath, '//div[@id="AdxReportViewImpl-TSV"]')
    # elt = this.wait_for(:link, ENV['REPORT_ID'])
    elt.click
    sleep 5

    logger.info "wait until downloading starts..."
    downloaded = false
    while true
      break if Dir.glob('/tmp/*.part').size > 0
      sleep 2
      file_report = Dir.glob("/tmp/#{report_name}.*.tsv").first
      if not file_report.nil? and File.stat(file_report).size != 0
        logger.info "Report <#{report_name}> downloaded!"
        downloaded = true
        break
      end
      logger.info "still waiting..."    
    end

    logger.info "download starts..."
    while true and not downloaded
      break if Dir.glob('/tmp/*.part').size == 0
      sleep 2
      logger.info "still downloading..."    
    end
    logger.info "done <#{Dir.glob('/tmp/*.tsv')}>"
    logger.info "Go back to the Query Tool"
  end
end






