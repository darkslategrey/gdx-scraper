
## Requirements

* Xvfb https://en.wikipedia.org/wiki/Xvfb


## Configuration

Make sure to create your '.env' file with theses variables:

```shell
SEMILLO_EMAIL=""
SEMILLO_PASS=""
EBAY_EMAIL=""
EBAY_PASS=""
CARET_CLASS=""
REPORT_ID=""
```

## Usage

```shell
$ bundle
$ ./xvfb-run ruby gdx_g.rb
```



