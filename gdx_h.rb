#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'

require 'json'
require 'selenium-webdriver'
require 'headless'
require 'pry'


class WebScraper
  include Capybara::DSL
  Capybara.default_driver = :poltergeist
  Capybara.register_driver :poltergeist do |app|
    options = { js_errors: false, debug: true, timeout: 180 }
    Capybara::Poltergeist::Driver.new(app, options)
  end

  def scrape
    session = Capybara::Session.new(:poltergeist)
    yield session
  end

  def self.scrape(&block)
    new.scrape(&block)
  end
end

WebScraper.scrape do |session|
  session.visit  "https://accounts.google.com/ServiceLoginAuth"
  session.save_screenshot

  session.fill_in('Email', with: 'semiloadxpilot@gmail.com')
  session.find('input#next').click
  sleep 3
  session.save_screenshot

  session.fill_in('Passwd', with: 'laboratoire')
  session.find('input#signIn').click
  sleep 3
  session.save_screenshot

  session.visit  "https://google.com/adx"
  sleep 5
  session.save_screenshot

  session.find('div#container-view-tab-QUERY_TOOL').click
  sleep 5
  session.save_screenshot

  # byebug
  begin
    session.find('div#WelcomeToQueryToolWidget-dismissTemporarilyButton').click
    sleep 10    
  rescue Exception => e
    puts "element not found"
  end
  session.save_screenshot

  # binding.pry

  ## reports name
  reports = session.find_all(:xpath, "//span[@class='lnk']")
  reports.each do |report|
    xpath   = report.path.split('/')[0..-5].push('TD[5]').join('/')
    puts "XPATH #{xpath}"
    last_td = session.find(:xpath, xpath)
    run_now_anchor = last_td.find_xpath('.//a').first
    run_now_anchor.click
    sleep 120
    session.save_screenshot
    carret = session.find("div.G6M3D1D-m-y.G6M3D1D-m-bb")
    carret.click
    sleep 2
    session.save_screenshot
    binding.pry    
    export_tsv = session.find(:xpath, '//div[@id="AdxReportViewImpl-TSV"]')
    export_tsv.click
    sleep 120
    session.save_screenshot    
    break
    # run_now_href   = a[:href]
  end
  
  # byebug
  # session.first('a.gwt-Anchor.G6M3D1D-m-q.G6M3D1D-m-v').click
  # sleep 10
  # session.save_screenshot

end









'https://www.google.com/adx/querytool/export?esqi=hcyhvfVPxp1IxkzFpfKr5K-wbOp9gtk60GLz3E3_kYOPYFZgku3EBNEPSulxFJCR&exi=2eee4877-b7a5-4727-8755-792d59efa800&run_as_of_date=2016-01-20&epfmt=TSV&role=seller&customerId=50137523&userId=154949333&authuser=0&shard=53'
